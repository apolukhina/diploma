#include <utility>
#include <vector>
#include <set>
#include <fstream>
#include "Polynomial/Polynomial.cuh"

#define BLOCK_SIZE 32
// Variable for master element in a column
__device__ int master_row_index;
__device__ int row_counter;


// GPU FUNCTION - search for the master element in a column
// --------------------------------------------------------------------------------------------------------------
__global__ void initialize(){
    master_row_index = -1;
    row_counter = 0;
}
__global__ void foundMasterRow(const int* matrix, int height, int width,  int iteration) {
    for (int i = row_counter; i < height; i ++){
        if (matrix[i * width + iteration]){
            master_row_index = i;
            return;
        }
    }
    master_row_index = -1;
}

__global__ void swapRows(int* matrix, int width){
    int swap_element;
    for (int j = 0; j < width; j ++){
        swap_element = matrix[row_counter * width + j];
        matrix[row_counter * width + j] = matrix[master_row_index * width + j];
        matrix[master_row_index * width + j] = swap_element;
    }
    row_counter ++;
}

__global__ void reduceRowsByMaster(int* matrix, int height, int width, int j){
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int idy = blockIdx.y * blockDim.y + threadIdx.y;
    if (idx < height && idy < width){
        if (master_row_index == idx ||  matrix[idx * width + j] == 0)
            return;
        else {
            if ((matrix[master_row_index * width + idy] && matrix[idx * width + idy]) || (!matrix[master_row_index * width + idy] && !matrix[idx * width + idy]))
                matrix[idx * width + idy] = 0;
            else
                matrix[idx * width + idy] = 1;
        }
    }
}

// FUNCTION FOR GPU - calculate echelon form matrix
// --------------------------------------------------------------------------------------------------------------
void reductionToRowEchelonForm(int* matrix, int height, int width){
    int* dev_matrix;
    // copy matrix from host to device
    cudaMalloc((void**)&dev_matrix, sizeof(int) * height * width);
    cudaMemcpy(dev_matrix, matrix, sizeof(int) * height * width, cudaMemcpyHostToDevice);
    dim3 dim_grid(height / BLOCK_SIZE + 1, width / BLOCK_SIZE + 1);
    dim3 dim_block(BLOCK_SIZE, BLOCK_SIZE);

    initialize<<<1, 1>>>();

    for (auto j = 0; j < width; j++){
        // found current master row
        foundMasterRow<<< 1,1 >>>(dev_matrix, height, width, j);
        // change current index of master row
        int master_index;
        cudaMemcpyFromSymbol(&master_index, master_row_index, sizeof(float), 0, cudaMemcpyDeviceToHost);
        if (master_index != -1){
            reduceRowsByMaster<<<dim_grid, dim_block>>>(dev_matrix, height, width, j);
            swapRows<<<1 , 1>>>(dev_matrix, width);
        }
    }
    cudaMemcpy(matrix, dev_matrix, sizeof(int) * height * width, cudaMemcpyDeviceToHost);
    cudaThreadSynchronize();
    cudaFree(dev_matrix);
}

// make matrix from coefficient of polynomials
// --------------------------------------------------------------------------------------------------------------
int* makeMatrix(const std::set<Polynomial>& F, const std::set<Monomial>& T) {
    auto matrix = new int [T.size() * F.size()];
    auto i = 0;
    for (auto poly_iter = F.begin(); poly_iter != F.end(); poly_iter++){
        auto monomials = poly_iter->getAllMonomials();
        auto j = 0;
        for (auto it = T.rbegin(); it != T.rend(); it++){
            if (monomials.count(*it) > 0)
                matrix[i*T.size() + j] = 1;
            else
                matrix[i*T.size() + j] = 0;
            j++;
        }
        i++;
    }
    return matrix;
}

// make polynomials from matrix
// --------------------------------------------------------------------------------------------------------------
std::set<Polynomial> makePolynomialsFromMatrix(const int* matrix, const std::set<Monomial>& T,
                                               int height, int width, int count_of_vars){
    std::set<Polynomial> F;
    for (auto i = 0; i < height; i++){
        std::map<Monomial, int> new_polynomial;
        for (auto j = 0 ; j < width; j++){
            if (matrix[i * width + j])
                new_polynomial[*std::next(T.rbegin(), j)] = 1;
        }
        if (!new_polynomial.empty())
            F.insert(Polynomial(new_polynomial, count_of_vars));
    }
    return F;
}
// updating function - Buchberger criteria, p 230
// --------------------------------------------------------------------------------------------------------------
void update_buchberger(std::set<Polynomial>& G, std::set<std::pair<Polynomial, Polynomial>>& P, const Polynomial& h){
    // initialize C,D
    std::set<std::pair<Polynomial, Polynomial>> C;
    std::set<std::pair<Polynomial, Polynomial>> D;
    // ----
    for (auto & g: G)
        C.insert({h,g});
    // -----
    while (!C.empty()){
        auto pair = *C.begin();
        auto g = pair.second;
        auto lcm_g_h = g.getHT().lcm(h.getHT());
        C.erase(*C.begin());
        // if HT(h) and HT(g) are disjoint
        if (g.getHT().lcm(h.getHT()) == g.getHT() * h.getHT()){
            D.insert(pair);
            continue;
        }
        auto need_add = true;
        for (auto & pair_2: C){
            auto g_2 = pair_2.second;
            auto lcm_g_2_h = g_2.getHT().lcm(h.getHT());
            if (lcm_g_2_h.is_divided(lcm_g_h)){
                need_add = false;
                break;
            }
        }
        if (!need_add)
            continue;
        for (auto & pair_2: D){
            auto g_2 = pair_2.second;
            auto lcm_g_2_h = g_2.getHT().lcm(h.getHT());
            if (lcm_g_2_h.is_divided(lcm_g_h)){
                need_add = false;
                break;
            }
        }
        if (need_add)
            D.insert(pair);
    }
    // -----
    std::set<std::pair<Polynomial, Polynomial>> E;
    while (!D.empty()){
        auto pair = *D.begin();
        auto g = pair.second;
        D.erase(*D.begin());
        if (g.getHT().lcm(h.getHT()) != g.getHT() * h.getHT())
            E.insert(pair);
    }
    // -----
    std::set<std::pair<Polynomial, Polynomial>> P_new;
    while (!P.empty()){
        auto pair = *P.begin();
        P.erase(*P.begin());
        auto g1 = pair.first;
        auto g2 = pair.second;
        auto lcm_g1_g2 = g1.getHT().lcm(g2.getHT());
        if (!h.getHT().is_divided(lcm_g1_g2) || h.getHT().lcm(g1.getHT()) == lcm_g1_g2 || h.getHT().lcm(g2.getHT()) == lcm_g1_g2)
            P_new.insert(pair);
    }
    // -----
    for (auto & pair: E)
        P_new.insert(pair);
    // -----
    std::set<Polynomial> G_new;
    while (!G.empty()){
        auto f = *G.begin();
        G.erase(*G.begin());
        if (!h.getHT().is_divided(f.getHT()))
            G_new.insert(f);
    }
    // -----
    G_new.insert(h);
    // -----
    P = P_new;
    G = G_new;
}

// updating function - Buchberger criteria
// --------------------------------------------------------------------------------------------------------------
void update(std::set<Polynomial>& G, std::set<std::pair<Polynomial, Polynomial>>& P,
            std::set<std::pair<Polynomial, Polynomial>>& used_P, const Polynomial& g){
    std::set<std::pair<Polynomial, Polynomial>> R;
    std::set<std::pair<Polynomial, Polynomial>> deleted_pairs;
    std::set<Polynomial> deleted_f;


    // if not LM(h) | LM(f) and LM(g) | lcm(LM(f), LM(h)) : P = P \ {f,h}
    for (auto & current_pair: P){
        auto first_poly = current_pair.first;
        auto second_poly = current_pair.second;
        auto lcm = first_poly.getHT().lcm(second_poly.getHT());
        if (!second_poly.getHT().is_divided(first_poly.getHT()) && g.getHT().is_divided(lcm))
            deleted_pairs.insert({current_pair});
    }

    // delete pairs from P
    for (auto& deleted_pair: deleted_pairs)
        P.erase(deleted_pair);

    // if LM(g) | LM(f) : G = G \ {f} and P = P + {f,g}
    for (auto & f: G){
        if (g.getHT().is_divided(f.getHT())){
            P.insert(std::pair<Polynomial, Polynomial>(f, g));
            deleted_f.insert(f);
        }
    }

    // delete extra g from G
    for (auto & f: deleted_f){
        G.erase(f);
    }

    // Buchberger criteria
    for (auto & f: G){
        auto lcm_f_g = f.getHT().lcm(g.getHT());
        bool need_add = true;
        // if g and f have engagement
        if (lcm_f_g != f.getHT()*g.getHT()){
            for (auto & h: G){
                if (h != f){
                    auto lcm_h_g = g.getHT().lcm(h.getHT());
                    auto lcm_f_h = f.getHT().lcm(h.getHT());
                    // if lcm(LM(h), LM(g)) | lcm(LM(f), LM(g))
                    bool first_condition = lcm_h_g.is_divided(lcm_f_g);
                    // if not LM(g) | lcm(LM(f), LM(h)) or ( LM(g) | lcm(LM(f), LM(h)) and {h,f}, {f,h} not in used_P)
                    bool second_condition = !(g.getHT().is_divided(lcm_f_h)) || (g.getHT().is_divided(lcm_f_h) && (used_P.count({f,h}) != 0 || used_P.count({h,f}) != 0));
                    if (first_condition && second_condition){
                        need_add = false;
                        break;
                    }
                }
            }
            if (need_add)
                R.insert({f,g});
        }
    }

    for (auto & current_pair: R){
        auto lcm = current_pair.first.getHT().lcm(current_pair.second.getHT());
        if (lcm != current_pair.second.getHT()*current_pair.first.getHT())
            P.insert(current_pair);
    }
    G.insert(g);
}
// reduce Groebner basis
// --------------------------------------------------------------------------------------------------------------
void reduce(std::set<Polynomial>& G){
    std::set<Polynomial> reduced_G;
    for (auto & f: G){
        auto need_add = true;
        for (auto & g: G){
            if (f != g && g.getHT().is_divided(f.getHT()))
                need_add = false;
        }
        if (need_add)
            reduced_G.insert(f);
    }
    std::cout << "REDUCED GROEBNER BASIS" << std::endl;
    for (auto & f: reduced_G){
        std::cout << f << std::endl;
    }
}

// simplify function
// --------------------------------------------------------------------------------------------------------------
Polynomial simpify(Monomial& m, const Polynomial& f, const std::vector<std::set<Polynomial>>& previous_F,
             const std::vector<std::set<Polynomial>>& row_echelon_F){
    for (auto & divisor: m.divisors()){
        for (auto j = 0; j < previous_F.size(); j ++){
            if (previous_F[j].count(f * divisor) > 0) {
                Polynomial p;
                auto current_echelon_form_F = row_echelon_F[j];
                for (auto & poly: current_echelon_form_F){
                    if (poly.getHT() == f.getHT() * divisor){
                        p = poly;
                        break;
                    }
                }
                if (divisor != m){
                    auto new_term = m / divisor;
                    return simpify(new_term, p, previous_F, row_echelon_F);
                }
                else
                    return p;
            }
        }
    }
    return f * m;
}

// symbolic preprocessing function for critical pairs
// --------------------------------------------------------------------------------------------------------------
std::set<Polynomial> symbolicPreprocessing(const std::set<Polynomial>& polynomials,
                                           const std::set<std::pair<Polynomial,Polynomial>>& critical_pairs,
                                           const std::vector<std::set<Polynomial>>& previous_F,
                                           const std::vector<std::set<Polynomial>>& row_echelon_F){
    // generate F
    std::set<Polynomial> F;
    for (auto & pair: critical_pairs){
        auto HT_1 = pair.first.getHT();
        auto HT_2 = pair.second.getHT();
        auto LCM = HT_1.lcm(HT_2);
        auto T_1 = LCM / HT_1;
        auto T_2 = LCM / HT_2;
        F.insert(simpify(T_2, pair.second, previous_F, row_echelon_F));
        F.insert(simpify(T_1, pair.first, previous_F, row_echelon_F));
        //F.insert(pair.second * T_2);
        //F.insert(pair.first * T_1);
    }
    // generate Done and set of all monomials from F
    std::set<Monomial> Done;
    std::set<Monomial> T;
    for (auto & polynomial: F) {
        Done.insert(polynomial.getHT());
        for (auto & monomial: polynomial.getAllMonomials())
            T.insert(monomial.first);
    }
    // generate set of terms without terms in Done
    std::set<Monomial> T_without_Done;
    for (auto & monomial: T){
        if (Done.count(monomial) == 0)
            T_without_Done.insert(monomial);
    }
    // prepare F for next computations (for matrix manipulations)
    while (T != Done){
        Monomial m = *T_without_Done.begin();
        Done.insert(m);
        for (auto & polynomial: polynomials){
            if (polynomial.getHT().is_divided(m)){
                auto _m = m / polynomial.getHT();
                F.insert(simpify(_m, polynomial, previous_F, row_echelon_F));
                break;
                //F.insert(polynomial * _m);
            }
        }
        T_without_Done.erase(m);
    }
    std::cout << "SYMBOLIC PREPROCESSING" << std::endl;
    for (auto & f: F)
        std::cout << f << std::endl;
    return F;
}


// reduction function for critical pairs
// --------------------------------------------------------------------------------------------------------------
struct reduction_return_type {
        std::set<Polynomial> F;
        std::set<Polynomial> echelon_F;
        std::set<Polynomial> result;
};

reduction_return_type reduction(const std::set<Polynomial>& polynomials,
               const std::set<std::pair<Polynomial, Polynomial>>& critical_pairs, int count_of_variables,
               const std::vector<std::set<Polynomial>>& previous_F,
               const std::vector<std::set<Polynomial>>& row_echelon_F){
    auto F = symbolicPreprocessing(polynomials, critical_pairs, previous_F, row_echelon_F);
    // make set of monomials for polynomials in F
    std::set<Monomial> T;
    std::set<Monomial> T_HT;
    for (auto & polynomial: F){
        T_HT.insert(polynomial.getHT());
        for (auto & monomial: polynomial.getAllMonomials())
            T.insert(monomial.first);
    }
    // make matrix
    auto matrix = makeMatrix(F, T);
    std::cout << "STARTED MATRIX" << std::endl;
    for (auto i = 0; i < F.size(); i++){
        for (auto j = 0; j < T.size(); j++)
            std::cout << matrix[i * T.size() + j];
        std::cout << std::endl;
    }
    // ROW ECHELON FORM
    reductionToRowEchelonForm(matrix, F.size(), T.size());
    std::cout << "FINISHED MATRIX" << std::endl;
    for (auto i = 0; i < F.size(); i++){
        for (auto j = 0; j < T.size(); j++)
            std::cout << matrix[i * T.size() + j];
        std::cout << std::endl;
    }
    // make polynomials from matrix
    auto F_echelon = makePolynomialsFromMatrix(matrix, T, F.size(), T.size(), count_of_variables);
    std::set<Polynomial> result_F;
    for (auto & f: F_echelon){
        auto cur_HT = f.getHT();
        if (T_HT.count(cur_HT) == 0)
            result_F.insert(f);
    }
    free(matrix);
    return {F, F_echelon, result_F};
}

// function for selecting pairs from set (normal select strategy)
// --------------------------------------------------------------------------------------------------------------
std::set<std::pair<Polynomial, Polynomial>> select(const std::set<std::pair<Polynomial, Polynomial>>& all_pairs){
    std::set<std::pair<Polynomial, Polynomial>> selected_pairs;
    int min_degree = -1;
    for (auto & pair: all_pairs){
        auto HT_1 = pair.first.getHT();
        auto HT_2 = pair.second.getHT();
        auto LCM = HT_1.lcm(HT_2);
        if ( min_degree > LCM.getDegree() || min_degree < 0 ) {
            min_degree = LCM.getDegree();
            selected_pairs.clear();
            selected_pairs.insert(pair);
        }
        else if ( min_degree == LCM.getDegree() )
            selected_pairs.insert(pair);
    }
    return selected_pairs;
}


// main function for generate Grebner basis
// --------------------------------------------------------------------------------------------------------------
void generateGrebnerBasis(const std::set<Polynomial>& list_of_polynomials, int count_of_variables){
    std::set<Polynomial> G;
    std::set<std::pair<Polynomial, Polynomial>> P;
    std::set<std::pair<Polynomial, Polynomial>> used_P;
    std::vector<std::set<Polynomial>> F;
    std::vector<std::set<Polynomial>> row_echelon_F;
    // add all polynomials in working set
    for (auto it = list_of_polynomials.rbegin(); it != list_of_polynomials.rend(); it++){
        update_buchberger(G, P, *it);
        //update(G, P, used_P, *it);
    }
    auto i = 0;
    while (!P.empty()){
        std::cout << i << " ITERATION" << std::endl;
        i ++;
        std::cout << "G" << std::endl;
        for (auto & f: G) {
            std::cout << f <<std::endl;
        }
        std::cout << "PAIRS" << std::endl;
        for (auto & p: P) {
            std::cout << p.first <<std::endl;
            std::cout << p.second <<std::endl;
            std::cout << "----" <<std::endl;
        }
        // select critical pairs
        auto critical_pairs = select(P);
        std::cout << "SELECTED PAIRS" << std::endl;
        for (auto & p: critical_pairs){
            std::cout << p.first <<std::endl;
            std::cout << p.second <<std::endl;
            std::cout << "----" <<std::endl;
        }
        // delete critical pairs from set of all pairs
        for (auto & critical_pair: critical_pairs){
            P.erase(critical_pair);
            used_P.insert(critical_pair);
        }
        // reduction by critical pairs
        auto result = reduction(G, critical_pairs, count_of_variables, F, row_echelon_F);
        F.push_back(result.F);
        row_echelon_F.push_back(result.echelon_F);
        auto result_F = result.result;

        // add new polynomials in our set G
        std::cout << "NEW POLYNOMIALS" << std::endl;
        for (auto it = result_F.rbegin(); it != result_F.rend(); it++){
            std::cout << *it << std::endl;
            update_buchberger(G, P, *it);
            // update(G, P, used_P, *it);
        }

    }
    reduce(G);
}


// start point of program
// -------------------------------------------------------------------------------------------------------------
int main() {
    int count_of_variables = 4;

    std::ifstream file("../test1.txt");
    std::string str;
    std::set<Polynomial> set_of_poly;
    while (std::getline(file, str))
        set_of_poly.insert(Polynomial(str, count_of_variables));

    generateGrebnerBasis(set_of_poly, count_of_variables);
    
    return 0;
}
