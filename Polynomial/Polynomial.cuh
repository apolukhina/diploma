#ifndef DIPLOMA_POLYNOMIAL_CUH
#define DIPLOMA_POLYNOMIAL_CUH

#include <string>
#include <vector>
#include <set>
#include <map>

#include "../Monomial/Monomial.cuh"

class Polynomial {
public:
    __host__ Polynomial(std::string poly_str, int count_of_vars);

    __host__ Polynomial(std::map<Monomial, int> poly_map, int count_of_vars);

    __host__ Polynomial();

    __host__  std::map<Monomial, int> getAllMonomials() const;

    __host__ bool operator< (const Polynomial& right) const;

    __host__ friend std::ostream& operator<< (std::ostream &out, const Polynomial &polynomial);

    __host__ bool operator!= (const Polynomial& right) const;

    __host__ Polynomial operator* (const Monomial& m) const;

    __host__ Polynomial divide() const;

    Monomial getHT() const;

private:
    std::map<Monomial, int> poly;
    int count_of_variables;
    Monomial HT;

    __host__ void _generatePolynomial(std::string poly_str);

};


#endif //DIPLOMA_POLYNOMIAL_CUH
