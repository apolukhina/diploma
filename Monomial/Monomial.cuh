#ifndef DIPLOMA_MONOMIAL_CUH
#define DIPLOMA_MONOMIAL_CUH

#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <map>


class Monomial {
public:
    __host__ Monomial(std::string monomial_str, int count_of_vars);

    __host__ Monomial(std::map<int,int> monomial_map, int count_of_vars);

     __host__ Monomial();

    __host__ Monomial lcm(const Monomial& right) const;

    __host__ std::set<Monomial> divisors() const;

    __host__ int getDegree() const;

    __host__ int degree_of_variable(int variable) const;

    __host__ Monomial divide_variables(std::vector<int> divide_vector) const;

    __host__ bool is_divided(const Monomial& m) const;

    __host__ friend std::ostream& operator<< (std::ostream &out, const Monomial &cut_monomial);

    bool operator< (const Monomial& right) const;

    bool operator> (const Monomial& right) const;

    bool operator== (const Monomial & right) const;

    bool operator!= (const Monomial & right) const;

    Monomial operator/ (const Monomial& divider) const;

    Monomial operator* (const Monomial& right) const;


private:
    std::map<int, int> monomial;
    int count_of_variables;
    int degree;

    __host__ std::map<int, int> _generateMonomial(std::string monomial_str);

    __host__ std::set<Monomial> _helpfulForDivisors(int iter_variable) const;

    __host__ int _computationDegree();

};


#endif //DIPLOMA_MONOMIAL_CUH
